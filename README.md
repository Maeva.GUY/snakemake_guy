# Snakemake Project

Author: Maëva GY

Date: 2024-01-02

System infos: Linux ws2023-master 3.10.0-1160.95.1.el7.x86_64 #1 SMP Mon Jul 24 13:59:37 UTC 2023 x86_64 x86_64 x86_64 GNU/Linux

## Automatic run

To have access to data and snakemake scripts, please enter the following command line in the @193.49.167.26 server.

```
git clone git@gitlab.com:Maeva.GUY/snakemake_guy
```

You have access to a bash file that you can run (while being in the home) using

```
bash workflow/ATAC-seq_smk.sh
```

## Manual run

However, if the script does not work, follow the instructions below to manually run the analysis :

```
cd snakemake_guy
mkdir log results
module purge
module load conda/4.12.0 
conda activate
module load gcc/8.1.0 python/3.7.1 snakemake/7.15.1
```

The following message should appear : 
Lmod is automatically replacing "conda/4.12.0" with "python/3.7.1".

This is normal and will not be a problem for the rest of the procedure.

The snakemake module is now available and usable.

To execute the scripts, enter the following command line:

```
snakemake --cores 8 --jobs 12 --use-conda --latency-wait 20 --snakefile workflow/1_Snakefile_qcinit.smk --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=log/slurm-%j.out"
```

This command line is used to run the first script (initial fastqc). To execute the other scripts, the only thing to change is the file name in the --snakefile option.
