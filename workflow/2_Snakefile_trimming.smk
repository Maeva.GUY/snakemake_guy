configfile: "/home/users/student17/snakemake_guy/config/config.yaml",

rule all:
    input:
        expand("results/trimming/{sample}_trimmed_1_fastq.gz", sample = config["compiled_samples"]),
        expand("results/trimming/{sample}_trimmed_2_fastq.gz", sample = config["compiled_samples"])

rule fastqc:
    input:
        forward = "/home/users/shared/data/stategra/atac-seq/{sample}_1.fastq.gz"
        reverse = "/home/users/shared/data/stategra/atac-seq/{sample}_2.fastq.gz"
    output:
        forward_paired = "results/trimming/trimmed_{sample}_1_paired.fastq.gz "
        forward_unpaired = "results/trimming/trimmed_{sample}_1_unpaired.fastq.gz"
        reverse_paired = "results/trimming/trimmed_{sample}_2_paired.fastq.gz"
        reverse_unpaired = "results/trimming/trimmed_{sample}_2_unpaired.fastq.gz"
    params:
        adapter_file = "adapters/TruSeq2-PE-2.fa"
    conda:
        "envs/trimming.yml"
    resources:
        mem_mb=5000,
        time="00:30:00"
    shell:
        """
        ml java
	java -jar /opt/apps/trimmomatic-0.38/trimmomatic-0.38.jar PE \
	-threads 8 \
	{input.forward} \
        {input.reverse} \
        {output.forward_paired} \
        {output.forward_unpaired} \
        {output.reverse_paired} \
        {output.reverse_unpaired} \
	ILLUMINACLIP:{params.adapter_file}:2:30:10 \
	LEADING:3 \
	TRAILING:3 \
	SLIDINGWINDOW:4:15 \
	MINLEN:33
        """
