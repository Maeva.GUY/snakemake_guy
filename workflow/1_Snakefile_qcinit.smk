configfile: "/home/users/student17/snakemake_guy/config/config.yaml",

rule all:
    input:
        expand("results/fastqc_init/{sample}_fastqc.html", sample = config["samples"]),
        
rule fastqc:
    input:
         expand("/home/users/shared/data/stategra/atac-seq/{sample}.fastq.gz", sample = config["samples"])
    output:
        expand("results/fastqc_init/{sample}_fastqc.zip", sample = config["samples"]), 
        expand("results/fastqc_init/{sample}_fastqc.html", sample = config["samples"])
    conda:
        "envs/qc.yml"
    resources: 
        mem_mb=5000, 
        time="00:30:00"
    shell: "fastqc --outdir results/fastqc_init/ {input}"

