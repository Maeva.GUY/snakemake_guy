mkdir log results
module purge
module load conda/4.12.0 
conda activate
module load gcc/8.1.0 python/3.7.1 snakemake/7.15.1

# Run the initial fastQC analysis before trimming
snakemake --cores 8 --jobs 12 --use-conda --latency-wait 20 --snakefile workflow/1_Snakefile_qcinit.smk --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=log/slurm-%j.out"

# Run the trimming analysis
snakemake --cores 8 --jobs 12 --use-conda --latency-wait 20 --snakefile workflow/2_Snakefile_trimming.smk --cluster "sbatch --mem {resources.mem_mb} --cpus-per-task {threads} --time={resources.time} --output=log/slurm-%j.out"
